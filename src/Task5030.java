import java.util.Arrays; 
import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Array;
import java.util.*;

public class Task5030 {
    public static void main(String[] args) throws Exception {
        Task5030 task = new Task5030();
        //task 1: check if string
        Object input = "Devcamp";
        if (input instanceof String){
            System.out.println("Task 1 - Input is string!");
        }
        else System.out.println("Task 1 - Input is not a string!");
        
        //task 2: Cut string
        String str = "Robin Singh";
        int n = 4;
        str = str.substring(0, n);
        System.out.println("Task 2 - String extracted with the first " + n + " characters: " + str);

        //task 3: split string add element to array
        String str3 = "Robin Singh";
        String[] words = str3.split("\\s+");
        System.out.println("Task 3 -Convert string to array of words: " + Arrays.toString(words));

        //task 4: replace space " " with dash "-"
        String input4 = "Robin Singh from USA";
        String output4 = input4.toLowerCase().replace(' ', '-');
        System.out.println("Task 4 - output: " + output4);

        //task 5: remove space and capitalise first char of words
        //khai báo input
        String input5a = "JavaScript Exercises";
        String input5b = "JavaScript exercises";
        String input5c = "JavaScriptExercises";
        //output: gọi hàm removeSpaceInString đã khai báo trong class Task5030
        Task5030.removeSpaceInString(input5a); 
        Task5030.removeSpaceInString(input5b);
        Task5030.removeSpaceInString(input5c);

        //task 6 - capitalised first letter
        String input6 = "js string exercises";
        String[] words6 = input6.split(" ");
        StringBuilder result6 = new StringBuilder();
        for (String word : words6) {
            result6.append(word.substring(0, 1).toUpperCase()).append(word.substring(1)).append(" ");
        }
        System.out.println("Task 6 - output: " + result6.toString().trim());

        //task 7 - create a string by repeat n substring
        String str7 = "Ha!";
        int n7 = 3;
        String result7 = Task5030.repeatWord(str7, n7);
        System.out.println("Task 7 - new string repeat " + str7 + " by " + n7 + " : " + result7);
        
        //task 8: convert string to array with elements are n char from string
        String str8 = "dcresource";
        int n8a = 2;
        int n8b = 3;
        String[] result8a = Task5030.splitString(str8, n8a);
        String[] result8b = Task5030.splitString(str8, n8b);
        System.out.println("Task 8 - new array separate by each " + n8a + " elements : " + Arrays.toString(result8a));
        System.out.println("Task 8 - new array separate by each " + n8b + " elements : " + Arrays.toString(result8b));

        //task 9
        String str9 = "The quick brown fox jumps over the lazy dog";
        String subString = "the";
        int lastIndex = 0;
        int count = 0;
        while(lastIndex != -1){
            lastIndex = str9.toLowerCase().indexOf(subString.toLowerCase(),lastIndex);
            if(lastIndex != -1){
                count ++;
                lastIndex += subString.length();
            }
        }
        System.out.println("Task 9 - number of substring in string (ignore upper/lower cases): " + count);

        //task 10: Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác
        String originalString = "0000";
        String newString = "123";
        int n10 = 3;
        String result10 = replaceRightMostCharacters(originalString, newString, n10);
        System.out.println("Task 10 - output after replacement: " + result10);

    }
    //task 5: function remove space and capitalise first char of words
    public static void removeSpaceInString(String str){
        //split string by space
        String[] words = str.split(" ");
        StringBuilder result = new StringBuilder();
        for (String word : words) {
            //capitalised 1st letter of word
            result.append(word.substring(0, 1).toUpperCase()).append(word.substring(1));
        }
        System.out.println("Task 5 - new string: " + result.toString());
    };
    //task 7: function to repeat substring
    public static String repeatWord(String word, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += word + " ";
        }
        return result.trim();
    }
    //task 8 - function to split string
    public static String[] splitString(String string, int n) {
        int arrayLength = (int) Math.ceil((double) string.length() / n);
        String[] result = new String[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            int startIndex = i * n;
            int endIndex = Math.min(startIndex + n, string.length());
            result[i] = string.substring(startIndex, endIndex);
        }
        return result;
    }
    //task 10 replace n element of original string with new string
    public static String replaceRightMostCharacters(String originalString, String newString, int n) {
        if (n > originalString.length()) {
          return newString;
        }
        return originalString.substring(0, originalString.length() - n) + newString;
      }

}
